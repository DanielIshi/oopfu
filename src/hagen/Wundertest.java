package hagen;

public class Wundertest {
    int counter = 0;

    int wunder(int a) {
        int i = 0;
        while (a > 1) {
            i++;
            if ((a % 2) == 0) a = a / 2;
            else {
                a = a * 3 + 1;
            }
        }
        return i;
    }


    int wunderRek(int wert, int counter) {
        counter++;
        wert = ((wert % 2) == 0) ?  wert / 2  : wert * 3 + 1;
        if (wert > 1) {   counter = wunderRek(wert, counter);  }
        return counter;
    }

    public static void main(String[] args) {
        Wundertest w = new Wundertest();

        for (int i = 0; i < 100; i++) {
            //System.out.println(i + ":" + w.wunder(i));
            System.out.println(i + ": "+  w.wunderRek(i, 0));
        }
    }

}
