package hagen;

public class Foo {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i = i + 2) {
            // Iteriere über die paarweise Argumente
            int ergebnis = 0;

                int x = Integer.parseInt(args[i]);
                int y = Integer.parseInt(args[i + 1]);
                x = x < 0 ? x * -1 : x; //Betrag von x
                y = y < 0 ? y * -1 : y; //Betrag von y
                ergebnis = x + y;
                System.out.println(ergebnis);
                while (ergebnis >= 3) {  // Bis ergebnis unter 3 , wird 3 von ergebnis abgezogen
                    ergebnis = ergebnis - 3;
                }
                // gibt den Modulo von ergebnis und 3 aus den Paaren aus
                switch (ergebnis) {
                    case 0:
                        System.out.println(i + 1 + ". Paar: 0");
                        break;
                    case 1:
                        System.out.println(i + 1 + ". Paar: 1");
                        break;
                    case 2:
                        System.out.println(i + 1 + ". Paar: 2");
                        break;
                    default:
                        System.out.println("Das kann nicht sein!");
                        break;

                }

        }
    }
}