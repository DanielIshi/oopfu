package hagen;



public class GameOfLife {
    class Zelle {
        final int X;
        final int Y;

        Zelle(int x, int y) {            X = x; Y = y;        }

        int gibLebendeNachbarn(){
            int summe=0;
            if (isRand()) return 0;
            summe+= feld[X - 1][Y] ? 1 :0; //darüber
            summe+= feld[X + 1][Y] ? 1 :0; //darunter
            summe+= feld[X][Y - 1]? 1 :0; //links
            summe+= feld[X][Y + 1]? 1 :0; //rechts
            summe+= feld[X - 1][Y - 1]? 1 :0; //oben links
            summe+= feld[X + 1][Y - 1]? 1 :0; //unten links
            summe+= feld[X - 1][Y + 1]? 1 :0; //oben rechts
            summe+= feld[X + 1][Y + 1]? 1 :0; //unten rechts
            return summe;
        }

        boolean isAlive() {             return isRand() ? false : (gibLebendeNachbarn() == 2 || gibLebendeNachbarn() == 3);   }
        private boolean isRand() {      return ((X == feld.length-1) || (X == 0) || (Y == feld.length-1) || (Y == 0));        }
    }



    void print() {
        for (int i = 0; i < feld.length; i++) {
            for (int j = 0; j < feld[i].length; j++) {
                if (feld[i][j]) {
                    System.out.print("o ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }

    void nextGeneration() {
        boolean[][] feldKopie = new boolean[feld.length][feld[0].length];

        for (int i = 0; i < feld.length ; i++)
            for (int j = 0; j < feld[0].length; j++)
                feldKopie[i][j] = feld[i][j];

        for (int i = 0; i < zellen.length; i++)
            for (int j = 0; j < zellen[0].length; j++)
                feldKopie[i][j] = (feld[i][j]) ? zellen[i][j].isAlive() : (zellen[i][j].gibLebendeNachbarn()==3) ;

        for (int i = 0; i < feld.length ; i++)
            for (int j = 0; j < feld[0].length; j++)
                feld[i][j] = feldKopie[i][j];
    }



    boolean[][] feld = {
            { false, false, false, false, false },
            { false, false, true, false, false },
            { false, false, true, false, false },
            { false, false, true, false, false },
            { false, false, false, false, false } };

    Zelle[][] zellen= new Zelle[feld.length][feld.length];

    GameOfLife() {
        for (int i = 0; i < zellen.length; i++) {
            for (int j = 0; j < zellen.length; j++) {
                zellen[i][j] = new Zelle (i,j);
            }
        }
    }

    public static void main(String[] args) {
        GameOfLife myGame = new GameOfLife();
        myGame.print();
        System.out.println();
        for (int i = 0; i < 10; i++) {
            myGame.nextGeneration();
            myGame.print();
            System.out.println();
        }
    }
}