package dritterLerntag.ea2;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Tier {
    int Beine = 0;
    String flugfaehig;
    String hatFedern;
    String gefaehrlichFuerMenschen;
    String Name;


    public String gibMeineEigenschaften() {
        String result = "";
        result = result + Beine + " Beine\n";
        result += "hat Federn: " + hatFedern+"\n";
        result += "flugfähig: " + flugfaehig+"\n";
        result += "gefährlich für Menschen: " + gefaehrlichFuerMenschen+"\n";
        return result;
    }


    public Tier(String name, int beine, String flugfaehig, String hatFedern, String gefaehrlichFuerMenschen) {
        Name = name;
        Beine = beine;
        this.flugfaehig = flugfaehig;
        this.hatFedern = hatFedern;
        this.gefaehrlichFuerMenschen = gefaehrlichFuerMenschen;
    }

    public static void main(String[] args) throws IOException {

        List<Tier> meineListe = new ArrayList<Tier>();

//        meineListe.iterator();
//        while ( meineListe.iterator().hasNext() ){
//            meineListe.iterator().next().gibMeineEigenschaften();
//        }
//        for (Tier aktuellerTier:meineListe) {
//            System.out.println(aktuellerTier.gibMeineEigenschaften());
//        }

        Tier Spinne = new Tier("Spinne", 8, "nein", "nein", "eventuell");
        Tier Hund = new Tier("Hund", 4, "nein", "nein", "eventuell");
        Tier Riesengorilla = new Tier("Riesengorilla", 2, "nein", "nein", "ja");
        Tier Giftschlange = new Tier("Giftschlange", 0, "nein", "nein", "ja");
        Tier Vogel = new Tier("Vogel", 2, "nein", "nein", "ja");

        Scanner scanner = new Scanner(System.in);
        String sTier = scanner.nextLine();

        if (sTier.equals("Spinne")) {
            System.out.println(Spinne.gibMeineEigenschaften());
        }
        if (sTier.equals("Hund")) {
            System.out.println(Hund.gibMeineEigenschaften());
        }
        if (sTier.equals("Riesengorilla")) {
            System.out.println(Riesengorilla.gibMeineEigenschaften());
        }
        if (sTier.equals("Giftschlange")) {
            System.out.println(Giftschlange.gibMeineEigenschaften());
        }
        if (sTier.equals("Vogel")) {
            System.out.println(Vogel.gibMeineEigenschaften());
        }


    }


}
