package dritterLerntag.ea2;

import java.util.Scanner;

public class TierImperativ {

    public static String printEigenschaften(String name, int Beine, String hatFedern, String flugfaehig, String gefaehrlichFuerMenschen) {
        String result = "";
        result = result + Beine + " Beine\n";
        result += "hat Federn: " + hatFedern+"\n";
        result += "flugfähig: " + flugfaehig+"\n";
        result += "gefährlich für Menschen: " + gefaehrlichFuerMenschen+"\n";
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sTier = scanner.nextLine();

        if (sTier.equals("Spinne")){  printEigenschaften("Spinne", 8, "nein", "nein", "eventuell");   }
        if (sTier.equals("Hund")){ printEigenschaften("Hund", 4, "nein", "nein", "eventuell"); }
        if (sTier.equals("Riesengorilla")){ printEigenschaften("Riesengorilla", 2, "nein", "nein", "ja"); }
        if (sTier.equals("Giftschlange")){printEigenschaften("Giftschlange", 0, "nein", "nein", "ja"); }
        if (sTier.equals("Vogel")){printEigenschaften("Vogel", 2, "nein", "nein", "ja");}

        if (sTier.equals("Mops")){  printEigenschaften("Mops", 8, "nein", "nein", "eventuell");   }
        if (sTier.equals("Ameise")){ printEigenschaften("Ameise", 4, "nein", "nein", "nein"); }
        if (sTier.equals("Schnecke")){ printEigenschaften("Schnecke", 2, "nein", "nein", "nein"); }
        if (sTier.equals("Schimpanse")){printEigenschaften("Schimpanse", 0, "nein", "nein", "nein"); }
        if (sTier.equals("Ente")){printEigenschaften("Ente", 2, "ja", "nein", "nein");}

    }
}

