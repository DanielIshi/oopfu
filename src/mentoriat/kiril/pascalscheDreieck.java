package mentoriat.kiril;

public class pascalscheDreieck {


    public static void main(String[] args) {
        int ebenen = 5;
        int[][] pascal = new int[ebenen][];

        for (int i = 0; i < pascal.length; i++) {
            pascal[i] = new int [i];
            for (int j = 0; j < i; j++) {
                if ( (j>0) && (j!=i) && (i>0) ) {
                    pascal[i][j] = pascal[i - 1][j - 1] + pascal[i - 1][j];
                }
                else {
                    pascal[i][j] = 1;
                }
            }
        }


        for (int i = 0; i < pascal.length ; i++) {
            for (int j = 0; j < i; j++) {
                System.out.println(pascal[i][j] + " ");
            }
            System.out.println();
        }


    }
}
