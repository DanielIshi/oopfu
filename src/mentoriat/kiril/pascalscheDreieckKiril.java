package mentoriat.kiril;

public class pascalscheDreieckKiril {


    public static void main(String[] args) {
        int ebenen = 5;
        int[][] pascal = new int[ebenen][];

        pascal[0] = new int [1];
        pascal[0][0] =1;

        pascal [1] = new int[2];
        pascal[1][0] = 1;
        pascal[1][1] = 1;

        for (int i = 2; i < ebenen; i++) {
            pascal[i] = new int [i+1];
            pascal[i][0] = pascal [i][i] = 1;
            for (int j = 1; j < i; j++) {
                pascal[i][j] = pascal[i-1][j] + pascal[i - 1][j-1];
            }
        }


        for (int i = 0; i < ebenen ; i++) {
            for (int j = 0; j < pascal[i].length; j++) {
                System.out.print(pascal[i][j] + " ");
            }
            System.out.println();
        }


    }
}
